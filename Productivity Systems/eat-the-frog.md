# Eat the Frog 🐸

> “If it’s your job to eat a frog, it’s best to do it first thing in the morning. And if it’s your job to eat two frogs, it’s best to eat the biggest one first.”
> – Mark Twain

## The Crux

Identify _one_ **important** task for the day and do it first.

## Why

* It promotes a deep work habit
* It ensures you’re setting your own agenda
* It sets you up to win
* It takes full advantage of your best work hours
* It’s dead simple and infinitely flexible

## The Method

1. **Decide on your frog**
   * Also known as your Most Important Task (MIT)
   * This task is usually important but not urgent
   * Generally creates mental resistance & ensuing procrastination
1. **Pick something you’ll be able to complete in 1–4 hours**
   * Aim for something that takes no longer than half of your day (ideally the first half)
   * A clearly defined, realistic task will make it easier to get started & not procrastinate
1. **Break it down into smaller steps if needed**
   * If your frog is larger than half-a-day, break off a 4-hour (or smaller) chunk
1. **Resist the temptation to plan ahead**
   * Don’t try to schedule out each frog, or chunk of frog, for the next few days or weeks
   * Accurately forecasting tasks into the future is nearly impossible
   * One of the benefits of EtF is that you get to start fresh with a singular focus each day
   * Identify & tackle your frogs one at a time!
1. **Prepare your frog the night before**
   * Plan just enough ahead so that you won’t get distracted when you sit down each morning
   * Get everything you need to get started in the morning set up at the end of each day
   * When you sit down in the morning, you shouldn’t need to go looking in emails, Slacks, issue boards, etc. for what your frog for the day is going to be
1. **Eat your frog first thing**
   * If at all possible, don’t schedule any meetings during your frog-eating time
   * Don’t even think about thinking about all the other less important (but quite likely **urgent**) tasks you have to get done
   * If you’re not a morning person, just make sure you schedule your frog for your most productive time of the day
