# Getting Things Done 📥

## 5 simple steps to a more productive life

1. Capture
   - Capture anything that crosses your mind – nothing is too big or small!
   - These items go directly in your “Inbox.”
2. Clarify
   - Process what you’ve captured into clear and concrete action steps.
   - You’ll decide if an item is a project, next action, or reference material.
3. Organize
   - Put everything in the right place:
     - Add dates to your calendar
     - Delegate action items
     - File away reference materials
     - Sort your tasks
4. Review
   - Frequently look over, update, and revise your lists.
   - Do smaller daily reviews and bigger weekly reviews.
5. Engage
   - Get to work on the important stuff.
   - User your system to know exactly what to work on when.

## Definitions

- **Inbox:** a visual representation of all the inputs you need to somehow deal with on a daily basis
  - Capture everything that crosses your mind!
    - To-dos, events, ideas, book recommendations, etc.
  - Not the place or time to worry about organization; simply collect everything!
- **Project:**
- **Next Action:**
- **Reference Material:**
- **Action Item:**
- **Delegation:**
