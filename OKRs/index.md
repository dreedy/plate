# Dallas’s Current OKRs

:point_right: **Current Quarter:** [FY21, Q3 →](fy21-q3.md)

---

These are the goals that I have set for myself – how I’d like to see myself grow. Each quarter is made up of primarily two sections:

- 🧭 Direction
  - Key indicators that I’m on the path I want to be on.
- 🗺 The Journey Ahead
  - Furthering some of the above objectives and/or key results, or perhaps diverting toward new paths as I discover new information about who I want to be.

---

## What an OKR is

**OKR** stands for **Objectives & Key Results**. It represents a framework for defining & tracking an **objective** – a clearly defined goal – & one or more **key results** – specific measures used to track the achievement of that goal. The main goal is to find a way to define _how_ each objective can be accomplished through concrete, specific, & measurable actions.

(Shamelessly copied from [Wikipedia](https://en.wikipedia.org/wiki/OKR).)

---

## See also

- [Dallas’s current tasks](../Tasks/2020-week-34.md)
- [Growth Sub-department](https://about.gitlab.com/handbook/engineering/development/growth/)
- [Acquisition, Conversion, and Backend Telemetry Group](https://about.gitlab.com/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/)
