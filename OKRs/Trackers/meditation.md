# Meditation Tracker

**Deprecated:** I have moved this tracker to [Notion](https://www.notion.so/Meditation-f43f7654b056436d980fb76aa5ad2f9c).

| W#  | Day | Date       | Duration |
| --- | --- | ---------- | -------- |
| 19  | Thu | 2020-05-07 | 20 min.  |
| 20  | Mon | 2020-05-11 |  5 min.  |
| 20  | Wed | 2020-05-13 |  5 min.  |
| 23  | Tue | 2020-06-02 |  5 min.  |
| 27  | Tue | 2020-06-30 |  5 min.  |
| 27  | Wed | 2020-07-01 | 10 min.  |
| 27  | Thu | 2020-07-02 |  5 min.  |
| 27  | Fri | 2020-07-03 | 10 min.  |
| 28  | Sun | 2020-07-05 | 10 min.  |
| 28  | Mon | 2020-07-06 |  5 min.  |

## Stats

| Meditations | Weeks | Avg/Wk |
| ----------- | ----- | ------ |
| 10          | 10    | 1      |
