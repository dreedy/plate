# Coffee Chats Tracker

**Deprecated:** I have moved this tracker to [Notion](https://www.notion.so/Coffee-Chats-4ac38a6ae85345988cdfa19061f3951d).

| W#  | Day | Date       | With Whom      |
| --- | --- | ---------- | -------------- |
| 19  | Fri | 2020-05-08 | Sujeevan V.    |
| 20  | Mon | 2020-05-11 | Sean C.        |
| 20  | Tue | 2020-05-12 | Christopher L. |
| 20  | Tue | 2020-05-12 | Hila Q.        |
| 20  | Fri | 2020-05-15 | Michael L.     |
| 21  | Tue | 2020-05-19 | Tucker L.      |
| 21  | Wed | 2020-05-20 | Emilie S.      |
| 22  | Tue | 2020-05-26 | Jay S.         |
| 22  | Tue | 2020-05-26 | Jamie C.       |
| 22  | Thu | 2020-05-28 | Matej L.       |
| 23  | Tue | 2020-06-02 | Vladlena S.    |
| 23  | Wed | 2020-06-03 | Nadia V.       |
| 23  | Thu | 2020-06-04 | Brie C.        |
| 23  | Thu | 2020-06-04 | Charlie A.     |
| 24  | Tue | 2020-06-09 | Patrick B.     |
| 24  | Wed | 2020-06-10 | Douglas A.     |
| 25  | Wed | 2020-06-17 | Kevin C.       |
| 25  | Thu | 2020-06-18 | Peter L.       |
| 26  | Tue | 2020-06-23 | Matthew B.     |
| 26  | Thu | 2020-06-25 | Carlos B.      |

## Stats

| Chats | Weeks | Avg/Wk |
| ----- | ----- | ------ |
| 20    | 10    | 2      |
