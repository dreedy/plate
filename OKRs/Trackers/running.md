# Running Tracker

**Deprecated:** I have moved this tracker to [Notion](https://www.notion.so/Running-2fb0a8abca7649ab93703ded60f3843b).

| W#  | Day | Date       | Distance |
| --- | --- | ---------- | -------- |
| 19  | Mon | 2020-05-04 | 2.46 mi. |
| 19  | Wed | 2020-05-06 | 2.18 mi. |
| 19  | Fri | 2020-05-08 | 2.38 mi. |
| 20  | Mon | 2020-05-11 | 3.41 mi. |
| 20  | Wed | 2020-05-13 | 3.41 mi. |
| 20  | Fri | 2020-05-15 | 3.38 mi. |
| 21  | Mon | 2020-05-18 | 3.42 mi. |
| 22  | Wed | 2020-05-27 | 3.29 mi. |
| 23  | Mon | 2020-06-01 | 2.35 mi. |
| 24  | Mon | 2020-06-08 | 3.12 mi. |
| 25  | Mon | 2020-06-15 | 3.09 mi. |
| 25  | Wed | 2020-06-18 | 3.65 mi. |
| 25  | Fri | 2020-06-19 | 3.88 mi. |
| 26  | Mon | 2020-06-22 | 3.47 mi. |
| 26  | Wed | 2020-06-24 | 3.70 mi. |
| 26  | Fri | 2020-06-26 | 2.68 mi. |

## Stats

| Distance  | Runs | Weeks | Avg Dist/Wk | Avg Dist/Run | Avg Runs/Wk |
| --------- | ---- | ----- | ----------- | ------------ | ----------- |
| 49.87 mi. | 16   | 10    | 4.99 mi.    | 3.12 mi.     | 1.6         |
