# Merge Requests

**Deprecated:** I have moved this tracker to [Notion](https://www.notion.so/Merge-Requests-2a07fb3fef70468d9eeb82190b1f9f63).

`WO#` refers to the week the merge request was opened and `WM#` refers to the week the merge request was merged. `Time to close` is business days (regardless of holidays or other time off) including the day it is opened & the day it is merged/closed.

| Merge Request                    | Status | WO# | Date Opened | WM# | Date Merged | Time to Close |
| -------------------------------- | ------ | --- | ----------- | --- | ----------- | ------------- |
| [gitlab-org/gitlab!31415][31415] | Merged | 19  | 2020-05-07  | 22  | 2020-05-27  | 15 days       |
| [gitlab-org/gitlab!32784][32784] | Merged | 21  | 2020-05-21  | 22  | 2020-05-28  | 6 days        |
| [gitlab-org/gitlab!33209][33209] | Merged | 22  | 2020-05-27  | 24  | 2020-06-12  | 13 days       |
| [gitlab-org/gitlab!33668][33668] | Merged | 23  | 2020-06-02  | 24  | 2020-06-10  | 7 days        |
| [gitlab-org/gitlab!33798][33798] | Merged | 23  | 2020-06-03  | 23  | 2020-06-03  | 45 minutes    |
| [gitlab-org/gitlab!33800][33800] | Merged | 23  | 2020-06-03  | 24  | 2020-06-08  | 4 days        |
| [gitlab-org/gitlab!33976][33976] | Merged | 23  | 2020-06-05  | 24  | 2020-06-12  | 6 days        |
| [gitlab-org/gitlab!34770][34770] | Open   | 25  | 2020-06-17  |     |             | 14 days…      |
| [gitlab-org/gitlab!35092][35092] | Merged | 26  | 2020-06-22  | 27  | 2020-07-01  | 8 days        |
| [gitlab-org/gitlab!35286][35286] | Open   | 26  | 2020-06-24  |     |             | 9 days…       |
| [gitlab-org/gitlab!35287][35287] | Open   | 26  | 2020-06-24  |     |             | 9 days…       |
| [gitlab-org/gitlab!35288][35288] | Open   | 26  | 2020-06-24  |     |             | 9 days…       |
| [gitlab-org/gitlab!35384][35384] | Open   | 26  | 2020-06-25  |     |             | 8 days…       |

## Stats

| Month | Opened | Merged |
| ----- | ------ | ------ |
| May   | 3      | 2      |
| June  | 10     | 5      |
| July  | 0      | 1      |

[31415]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/31415
[32784]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/32784
[33209]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33209
[33668]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33668
[33798]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33798
[33800]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33800
[33976]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33976
[34770]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34770
[35092]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35092
[35286]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35286
[35287]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35287
[35288]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35288
[35384]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35384

---

**Data source:** [merge requests by me](https://gitlab.com/dashboard/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&author_username=dreedy)
