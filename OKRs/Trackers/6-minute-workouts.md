# 6-Minute Workout Tracker

**Deprecated:** I have moved this tracker to [Notion](https://www.notion.so/6-Minute-Workouts-fc427d17a66341868c797a8fb67bc46c).

| W#  | Day | Date       |
| --- | --- | ---------- |
| 19  | Mon | 2020-05-04 |
| 19  | Wed | 2020-05-06 |
| 20  | Mon | 2020-05-11 |
| 20  | Tue | 2020-05-12 |
| 20  | Wed | 2020-05-13 |
| 23  | Wed | 2020-06-03 |
| 25  | Sun | 2020-06-14 |
| 26  | Mon | 2020-06-22 |
| 26  | Tue | 2020-06-23 |
| 26  | Wed | 2020-06-24 |
| 27  | Mon | 2020-06-29 |

## Stats

| Workouts | Weeks | Avg/Wk |
| -------- | ----- | ------ |
| 11       | 10    | 1.1    |
