# Tracking Mistakes & Learnings

**Deprecated:** I have moved this tracker to [Notion](https://www.notion.so/Mistakes-Learnings-2f897c0a05944e65acadda0a2fe8a540).

| W#  | Day | Date       | Mistake | Learning |
| --- | --- | ---------- | ------- | -------- |
| 21  | Mon | 2020-05-18 | [Asking if a review thread could be resolved rather than just resolving it myself.](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/31415#note_344792145) | It’s okay, maybe even encouraged, to resolve somebody else’s question/comment/feedback/suggestion thread if you feel like it truly is resolved. |
| 22  | Tue | 2020-05-26 | Added new commits (rebased, force-pushed) & comments after review had finished and MR was set to “merge when pipeline succeeds (MWPS),” thereby aborting the MWPS. | Read through the activity log as well as the comments to see what state an MR is really in. |
| 26  | Tue | 2020-06-30 | (From 360 Review feedback) Spinning my wheels / doing nothing / waiting too long for help but not doing my due diligence of research in the meantime. | Find ways to ask my questions “out loud” and then start digging into the answers on my own before reaching out to others. |
| 26  | Tue | 2020-06-30 | Asking questions (in meetings or in Slack) about changing/adjusting/fixing things when a simple MR would ask the question, provide a potential answer, and open it up to discussion all at once. | Just take the time to make the MR then bring its existence to the right people’s attention. |
