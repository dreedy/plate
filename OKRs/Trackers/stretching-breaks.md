# Stretching Break Tracker

**Deprecated:** I have moved this tracker to [Notion](https://www.notion.so/Stretching-Breaks-ee360362d810400f8c05667515b9b34d).

I’m choosing to count the before & after stretches every time I go for a run as part of this.

| W#  | Day | Date       | # of Times |
| --- | --- | ---------- | ---------- |
| 19  | Mon | 2020-05-04 | 2          |
| 19  | Wed | 2020-05-06 | 3          |
| 19  | Fri | 2020-05-08 | 3          |
| 20  | Mon | 2020-05-11 | 3          |
| 20  | Wed | 2020-05-13 | 3          |
| 20  | Fri | 2020-05-15 | 3          |
| 21  | Mon | 2020-05-18 | 4          |
| 21  | Tue | 2020-05-19 | 1          |
| 21  | Wed | 2020-05-20 | 1          |
| 22  | Wed | 2020-05-27 | 3          |
| 22  | Thu | 2020-05-28 | 2          |
| 22  | Fri | 2020-05-29 | 2          |
| 23  | Mon | 2020-06-01 | 3          |
| 23  | Tue | 2020-06-02 | 1          |
| 24  | Mon | 2020-06-08 | 5          |
| 24  | Tue | 2020-06-09 | 1          |
| 24  | Wed | 2020-06-10 | 1          |
| 24  | Thu | 2020-06-11 | 3          |
| 25  | Mon | 2020-06-15 | 9          |
| 25  | Tue | 2020-06-16 | 5          |
| 25  | Wed | 2020-06-17 | 5          |
| 25  | Thu | 2020-06-18 | 5          |
| 25  | Fri | 2020-06-19 | 4          |
| 26  | Mon | 2020-06-22 | 6          |
| 26  | Tue | 2020-06-23 | 4          |
| 26  | Wed | 2020-06-24 | 4          |
| 26  | Thu | 2020-06-25 | 2          |
| 26  | Fri | 2020-06-26 | 3          |
| 27  | Mon | 2020-06-29 | 3          |
| 27  | Tue | 2020-06-30 | 4          |
| 27  | Wed | 2020-07-01 | 3          |
| 27  | Thu | 2020-07-02 | 2          |
| 27  | Fri | 2020-07-03 | 1          |
| 28  | Mon | 2020-07-06 | 2          |

## Stats

| Breaks | Days | Weeks | Avg/Day | Avg/Wk |
| ------ | ---- | ----- | ------- | ------ |
| 106    | 46   | 10    | 2.30    | 10.6   |
