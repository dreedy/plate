# FY##.Q# — MONTH–MONTH, 2020

## 🧭 Direction

Key indicators that I’m on the path I want to be on.

- [ ] **An objective**
  - [ ] Key result #1
  - …

---

## 🗺 The Journey Ahead

Furthering some of the above objectives and/or key results, or perhaps diverting toward new paths as I discover new information about who I want to be.

- Something to strive for in the future
- …
