# 2020.W# — DATE–DATE

[Milestone ##.#][%##.#] – W#

## 🙋 Weekly Commitment

High-level tasks that I have every intention of working on this week.

- [ ] …

### Weekly Reflections

---

## 🙆 Daily tasks

First the planned, and later the actual, tasks that I accomplish each day & the amount of time each takes.

### Monday

150 / 425 min

- [ ] (50 min) Perform MR reviews (as needed)
- [ ] (25 min) Eng. Week in Review
- [ ] (50 min) Validation Track Work
- [ ] (25 min) Wrap up today, plan tomorrow

#### Monday Reflections

### Tuesday

225 / 475 min

- [ ] (25 min) Frontend Dept. Meeting
- [ ] (50 min) Perform MR reviews (as needed)
- [ ] (25 min) 1:1 w/ Jerome
- [ ] (50 min) Growth Eng. Weekly
- [ ] (50 min) Validation Track Work
- [ ] (25 min) Wrap up today, plan tomorrow

#### Tuesday Reflections

### Wednesday

175 / 425 min

- [ ] (50 min) Pair programming w/ Nicolas
- [ ] (50 min) Perform MR reviews (as needed)
- [ ] (50 min) Validation Track Work
- [ ] (25 min) Wrap up today, plan tomorrow

#### Wednesday Reflections

### Thursday

275 / 475 min

- [ ] (50 min) Coffee chat w/ Alex & Nicolas
- [ ] (25 min) Growth Conversion Team Meeting
- [ ] (50 min) Perform MR reviews (as needed)
- [ ] (25 min) 1:1 w/ Sam
- [ ] (50 min) Pairing w/ Jerome
- [ ] (50 min) Validation track work
- [ ] (25 min) Wrap up today, plan tomorrow

#### Thursday Reflections

### Friday

150 / 200 min

- [ ] (25 min) :frog: Work on blog post
- [ ] (25 min) Perform MR reviews (as needed)
- [ ] (25 min) Do some learning
- [ ] (25 min) Review group conversation agendas/notes from the week
- [ ] (25 min) Validation track work
- [ ] (25 min) Wrap up this week, plan next

#### Friday Reflections

---

## 💁 To do later

Tasks which have come up during the week but which I am giving myself permission to avoid doing at all this week. Checkboxes here signify that these items have been moved to my [task backlog][backlog].

- [ ] …

---

## See also

- [Growth:Conversion Issue Board]
- [Growth:Conversion Epics Roadmap]

[%##.#]: https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=dreedy&milestone_title=##.#
[backlog]: backlog.md
[Growth:Conversion Issue Board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion
[Growth:Conversion Epics Roadmap]: https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aconversion
