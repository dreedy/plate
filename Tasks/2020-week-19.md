# 2020.W19 — May 4–8

[Milestone 13.0](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=dreedy&milestone_title=13.0) – W3

- [x] Begin work on [issue #5363](https://gitlab.com/gitlab-org/growth/engineering/-/issues/5363)
  - [x] Create a branch
  - [x] Make first commit
  - [x] Start an MR
- [x] Watch more Cybertec PostgreSQL training session recordings (they expire May 11)
  - [x] Session 3
  - [x] Session 4
  - [x] Session 5
- [x] Move tasks over from my Google Tasks lists
  - See notes on this below.
  - [x] Step 1: pull all Google Tasks items into the “To do later” section
  - [x] Step 2: schedule or ruthlessly delete each one!
- [x] Add some of my thoughts about the Google Tasks migration to my main `README`.
- [x] Complete at least 5 tasks from my onboarding issue. (7/5)
- [x] Create a Trackers directory under OKRs & start tracking things there
- [x] Start working through the list of issues that the GitLab Bot found for my MR

---

## Unplanned tasks

- [x] Start Feature Flag training

---

## To do later

The checkboxes here are for indicating that these tasks have been scheduled elsewhere.

- [x] (15 min) Consider [shadowing an on-call engineering shift](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#shadowing-a-whole-shift) as I get closer to becoming eligible (2–3 months out)
- [x] (5 min) Let Clement Ho know how much I enjoyed the technical interview process (next week)
- [x] (20 min) Watch Susan Cain’s TED talk on [“The Power of Introverts”](https://www.ted.com/talks/susan_cain_the_power_of_introverts?language=en) (TBD)
- [x] (3 hr) Read the [Halloway Guide to Equity Compensation](https://www.holloway.com/g/equity-compensation) (re: Stock Options) (TBD)
- [x] (30 min) Review [the Q/A doc](https://docs.google.com/document/d/1HWNifdWRG6Mm3lZjV_Tlqj_yLoWo2aamYkql_A2pPJY/edit#heading=h.mxhyv91rino6) from the Stock Options session (TBD)
- [x] (1 hr) Read up on [GitLab’s stock option grants](https://about.gitlab.com/handbook/stock-options/) (TBD)
- [x] (20 min) [Learn about 401(k) plans](https://www.investopedia.com/terms/1/401kplan.asp) from a high level (TBD)
- [x] (5 min) Review [GitLab’s 401(k) plan](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#401k-plan); reach out on [#total-rewards](https://gitlab.slack.com/archives/CTVK60M32) if I still need help understanding it (TBD)
- [x] (30 min) Do an overview of the [Career Development Framework](https://about.gitlab.com/handbook/engineering/career-development/) (TBD)
- [x] (5 min) Schedule a coffee chat with Hila Qu (Director of Product, Growth) (next week)
- [x] (5 min) Schedule a coffee chat with Christopher “Leif” Lefelhocz (Sr. Director of Development) (next week)
- [x] (10 min) Schedule 1+ coffee chat(s) with member(s) of the PeopleOps (handbook writing) team (in 2 weeks)
- [x] (15 min) Review the docs on [GitLab Quick Actions](https://docs.gitlab.com/ee/user/project/quick_actions.html) (next week)
- [x] Finish up my work on [growth/engineering#5363](https://gitlab.com/gitlab-org/growth/engineering/-/issues/5363) (next week)
- [x] Continue [my Feature Flag training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7649) (next week)
- [x] Continue [my Iteration training](https://gitlab.com/gitlab-com/Product/-/issues/1060) (next week)

---

## Migrating from Google Tasks

Where should they all live, especially if they are “at some point” type tasks?

Here are my thoughts:

- If something is important enough to write down and keep track of, it’s important enough to schedule
- If something would be scheduled out beyond this week, there are two options:
  1. Add it to the “later” tasks list (with whatever date I expect to be able to get to it), and then forward it along through each week until we arrive at the scheduled week
  1. Just make the file for whichever week and add it in as a task

The second option would mean that I’ll either have to look for existing weeks as I go along and copy their contents into the `index` when I arrive at that week or that I’ll have to simply point to the current week from the `index` file. I wish I could just include the contents of another file as a Markdown partial or something, that would be great! (Doesn’t look like that’s a thing. 😞)

One of the main goals is to avoid some giant, never-ending task backlog, either within the current week’s “To do later” section, constantly being carried over week to week and growing in size, or in a separate `backlog.md` type file, also just growing in size and causing most tasks to be completely burried & lost. Either a task should be scheduled as “I plan to work on this” or it should be completely discarded (it can always come back later if it turns out to be important).

---

## See also

- [Growth:Conversion Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
