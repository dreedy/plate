# Task Management

:point_right: **Current Week:** [2020, W34 →](2020-week-34.md)

---

This is where I track all of the tasks I plan to get done each week. A weekly task tracking file is broken down into four main sections:

- 🙋 Commitment
  - All the tasks I have every intention of completing this week.
- 🙆 Unplanned tasks
  - Things that arose throughout the week that captured my interest or demanded my attention and which I felt the need or capacity to do now.
- 💁 To do later
  - Tasks which I am giving myself permission to avoid doing at all this week. The checkboxes here are for indicating that these tasks have been scheduled elsewhere.
- 🙅 Tasks to let go of
  - Tasks that I may have been forwarding along or which I may have initially added to the “To do later” list which I no longer plan to do – if they happen, great, but I’m not actively planning to complete them.

---

## See also

- [Growth:Conversion Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
- [Dallas’s current OKRs](../OKRs/fy21-q3.md)
