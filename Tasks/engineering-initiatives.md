# Engineering Initiatives

Non-product-driven initiatives which I am thinking about or tracking.

- Create issue addressing side-effects of `namespace.actual_plan`

- Feature spec cleanup work related to [#219944]
  - I believe I have a `git stash` for this effort

- Look into the creation of a `BillingPlans` object (or similarly named) which knows what to do with a group of `BillingPlan` objects (see the `Hashie::Mash` object returned from `FetchSubscriptionPlansService`)
  - The reason I feel like this would be helpful is that there seems to be a lot of logic duplication going on between the controllers (`ee/app/controllers/{groups,profiles}/billings_controller.rb`), views (`ee/app/views/shared/billings/*`), and helper (`ee/app/helpers/billing_plans_helper.rb`), and I feel like a lot of it could go away with some good object-level caching of, for example, which plan is the “current” plan, which ones are “upgradeable” or “downgradeable”, etc.
  - As a note, the proposed `BillingPlans` object would also need to know the current target `namespace` (and whether it is for a `User` or a `Group`)

- Look into [creating a simple Slack app](https://api.slack.com/) that reports your own Slack statistics (e.g. how many messages sent, how many DMs sent, how many DMs read, etc.)

- Investigate why we chose to build our own A/B testing framework on top of Flipper using the `percentage_of_time` gate & manually converting that to a percentage-of-actors-like gate rather than using Flipper’s built-in `percentage_of_actors` gate.
  - [Flipper’s available gates](https://github.com/jnunemaker/flipper/blob/master/docs/Gates.md)
  - [MR that started the A/B framework](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/17703)
  - Is there an issue discussing the options?
    - Yes – there are a few:
      - [growthproject#79](https://gitlab.com/gitlab-org/growthproject/-/issues/79)
      - [growthproject#78](https://gitlab.com/gitlab-org/growthproject/-/issues/78) which moved to [growth/engineering#5](https://gitlab.com/gitlab-org/growth/engineering/-/issues/5)
      - A similar effort on the Customers app, but this time they chose to use Unleash & dogfood GitLab’s own Feature Flag feature: [issue](https://gitlab.com/gitlab-org/growth/engineering/-/issues/117) | [MR](https://gitlab.com/gitlab-org/customers-gitlab-com/-/merge_requests/758)
    - I had some success finding these by searching for `Label=~devops::growth A/B test`
