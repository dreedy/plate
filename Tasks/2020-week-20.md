# 2020.W20 — May 11–15

[Milestone 13.0](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=dreedy&milestone_title=13.0) – W4

- [ ] Finish up my [first MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/31415) for [growth/engineering#5363](https://gitlab.com/gitlab-org/growth/engineering/-/issues/5363)
  - [x] Finish working through the list of issues that the GitLab Bot found for my MR
  - [ ] Set `user_preferences` column when user clicks one or the other option
  - [ ] Advance to user’s dashboard after user clicks an option
  - [x] Add tests for the first iteration, super-simple version
- [ ] Start work on iteration 2 of [growth/engineering#5363](https://gitlab.com/gitlab-org/growth/engineering/-/issues/5363)
  - [ ] Need to make entire option area (currently a `<div>`) clickable
  - [ ] Accept `group_id` param
  - [ ] Advance to group page after user clicks an option
  - [ ] Add basic styling
  - [ ] Add images
- [ ] (5 min) Let Clement Ho know how much I enjoyed the technical interview process
- [x] (5 min) Schedule a coffee chat with Hila Qu (Director of Product, Growth)
- [x] (5 min) Schedule a coffee chat with Christopher “Leif” Lefelhocz (Sr. Director of Development)
- [x] Continue [my Feature Flag training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7649)
- [x] Continue [my Iteration training](https://gitlab.com/gitlab-com/Product/-/issues/1060)
- [ ] Create a script that will copy the `_template.md` file and name it according to a given `year` and `week`
  - something like `> ./Tasks/new-week 2020 20`, which would have created this file
- [ ] (15 min) Review the docs on [GitLab Quick Actions](https://docs.gitlab.com/ee/user/project/quick_actions.html)

---

## Unplanned tasks

- [x] [Helped](https://gitlab.slack.com/archives/C038E3Q6L/p1589506841172400) (Slack link, so it’ll decay eventually) [@Josh_Zimmerman](https://gitlab.com/Josh_Zimmerman) get [two](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/47858#note_342920930) [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/48355#note_342925901) pipelines passing & ready to merge
- [x] Helped [@oregand](https://gitlab.com/oregand) dig deeper on an [issue with failing specs](https://gitlab.slack.com/archives/C0AR2KW4B/p1589472546188100) (Slack link, so it’ll decay eventually).
- [x] Tried to fix the tooling/overcommit/Makefile, but gave up after way too much effort :facepalm:

---

## To do later

The checkboxes here are for indicating that these tasks have been scheduled elsewhere.

- [ ] …

---

## See also

- [Growth:Conversion Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
