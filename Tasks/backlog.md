# Backlog of Tasks

I know I said I didn’t want one or that I’d never do one, but it’s starting to make sense to me to just keep my entire backlog of tasks in one place. And this is that place.

- [ ] Read through the [Telemetry Guide](https://docs.gitlab.com/ee/development/telemetry/index.html)
- [ ] Read through the [Usage Ping Guide](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html)
- [ ] Read through the [Snowplow Guide](https://docs.gitlab.com/ee/development/telemetry/snowplow.html)
- [ ] Check out the [GitLab 101 Tool Certification](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/gitlab-101/)
- [ ] Read the [Career Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/) page
- [ ] Look over the [Sr. Engineer Coaching Template](https://docs.google.com/document/d/11xZpY2RuTldp1g6bHRFYKwwlQjNjYaPquLPo5uD6hrg/edit#)
- [ ] Review the page on [Promotions & Transfers](https://about.gitlab.com/handbook/people-group/promotions-transfers/)
- [ ] Review some of the [GitLab Competencies](https://about.gitlab.com/handbook/competencies/#list)
  - [x] Iteration (completed on 2020-07-01)
- [ ] Check my Carta access (stock options)
  - I think I’ll receive an email, actually, when my stock options have been approved and my access to Carta is granted.
- [ ] Read the [Halloway Guide to Equity Compensation](https://www.holloway.com/g/equity-compensation) (re: Stock Options)
- [ ] Review [the Q/A doc](https://docs.google.com/document/d/1HWNifdWRG6Mm3lZjV_Tlqj_yLoWo2aamYkql_A2pPJY/edit#heading=h.mxhyv91rino6) from the Stock Options session
- [ ] Read up on [GitLab’s stock option grants](https://about.gitlab.com/handbook/stock-options/)
- [ ] Check in on this discussion about [The relationship between Feature Flags and Docs](https://gitlab.com/gitlab-org/gitlab/-/issues/241034)
