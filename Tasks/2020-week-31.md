# 2020.W31 — July 27–31

[Milestone 13.3](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=dreedy&milestone_title=13.3) – W2

## 🙋 Weekly Commitment

High-level tasks that I have every intention of working on this week.

- [x] Get caught up after my vacation
  - [x] Review emails
  - [x] Review Slack messages
  - [x] Review GitLab conversations
  - [x] 1:1 w/ Sam on [%13.3] priorities
- [ ] Get [Omit non-free namespaces from trial select (!36394)][!36394] pushed through
- [ ] ~~Begin work on [Confidential trial-related issue (!723) (internal)][!723]~~
  - Hold off on this until after planned [%13.3] items are completed
- [x] Begin work on [[ENG] Add contact sales option in app (#219944)][#219944]
- [x] Take a look at [my productivity process (internal)][my workflow]
  - [x] Review my time allocation
    - I should be spending 50% or more of my time on Build track work (i.e. writing code and pushing MRs through)
  - [x] Find better mix of this Plate project & Status Hero daily updates
  - [x] Make time to reflect on each day & each week – how am I doing toward the goals I’ve set for myself?
    - I have the time scheduled (last half-hour of each day), I just need to make sure that I use it for that purpose!

---

## 🙆 Daily tasks

First the planned, and later the actual, tasks that I accomplish each day (as recorded in [Status Hero (internal)][status hero]).

### Monday – 8.5 hours

- [x] Meet w/ Sam re: [%13.3] priorities
- [x] Review the three projects that Sam would like me to look at for [%13.3]
- [x] Catch up on emails, Slacks, & GitLab conversations
- [x] Review minutes and/or video recordings of last week’s key meetings
- [x] Figure out my Dev On-Call schedule assignments
- [x] Sign up for Dev On-Call time slots in September
- [x] Schedule time to catch up with Alex on New User Onboarding work
- [x] Schedule coffee chat for donut pairing

### Tuesday – 9.5 hours

- [x] FE Dept. Meeting
- [x] Catch up with Alex
- [x] 1:1 w/ Jerome
- [x] Growth Eng. Weekly
- [ ] ~Coaching session~ _(rescheduled for next week)_
- [x] Continue getting caught up with emails & GitLab.com conversations
- [x] Dive back into work on [Omitting non-free namespaces from trial select options (!36394)][!36394]
- [x] Prepare for workflow conversation w/ Jerome
- [x] Continue working through stack of interesting things from last week

### Wednesday – 8.5 hours

- [x] Workflow deep dive w/ Jerome
- [x] :frog: Get [Omitting non-free namespaces from trial select options (!36394)][!36394] ready for maintainer review
- [ ] Continue working through stack of interesting things from last week _(I managed to add to it… does that count?)_
- [x] Recover all of my [lost Chrome tabs (internal)](https://gitlab.slack.com/archives/C0259241E/p1596036984311100) from overnight update :cry:
  - :man_facepalming: turns out that OneTab had already saved them all for me
- [x] Add a ~unplanned label that I can use to tag tasks with
- [x] Adjust my schedule to include 1 hour each day for Validation track work
- [x] Adjust my schedule to include 1 hour each day for doing MR Reviews
- [x] Finally make a new week for this week in my Plate project

### Thursday – 9.5 hours

- [x] :frog: (broken time throughout the day) Begin work on [[ENG] Add contact sales option in app (#219944)][#219944]
- [x] (15 min) Keep pushing [Omitting non-free namespaces from trial select options (!36394)][!36394] through to merge
- [x] (60 min) 1:1:1 w/ Alex & Nicolas
- [x] (30 min) Growth Conversion Team Meeting
- [x] (30 min) 1:1 w/ Sam
- [x] (60 min) Pairing deep dive w/ Jerome
- [x] (30 min) Skip-level w/ Bartek
- [x] (15 min) Sign up for a timeslot during our next [Growth Team Day] to host another escape room adventure (I’m thinking 9am Pacific would work well)

### Friday – 4 hours

- [x] :frog: (30 min) Work on an outline for my blog post
- [x] (10 min) Update my OKR trackers
- [x] (10 min) Add myself to the reviewer rotation for Backend MRs
  - [Code Review › Reviewer](https://about.gitlab.com/handbook/engineering/workflow/code-review/)
  - [Engineering Division section of my onboarding issue](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/1940#engineering-division)
- [x] (10 min) Open an issue to become a trainee maintainer of the backend specializing in Ruby on Rails
- [x] (2 hr) Do Validation & Review work
- [x] (15 min) Look at failing pipeline for [Omit non-free namespaces from trial selection options (!36394)][!36394] to see why it’s failing
- [x] (45 min) Go through Slack messages (wow, that takes up some time!)
- [ ] (10 min) Wrap up this week, prepare for next week

---

## 💁 To do later

Tasks which I am giving myself permission to avoid doing at all this week. This is essentially my ongoing backlog of things I still want to get to at some point. Checkboxes here signify that these items have been forwarded or scheduled.

- [x] (10 min) Look into [volunteering to teach/share knowledge with underrepresented groups][#8516]
- [x] (60 min) Review group conversation agendas/notes from the week
- [x] (15 min) See if you can find an answer/solution/work-around/reason/etc. to this:
  > WARNING: sha_attribute :verification_checksum is invalid since the table doesn't exist - you may need to run database migrations
- [x] Consider [shadowing an on-call engineering shift](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#shadowing-a-whole-shift) as I get closer to becoming eligible
- [x] Review updated information for [[ENG] alternate teammates invitation in first time onboarding (#219544)](#219544) and finish the planning breakdown for it
- [x] Finish Feature Flag Training (see OneTab entry)
  - Along with your Feature Flag Training, ensure you also review the following:
    - [x] Information [about the Feature Flags feature](https://about.gitlab.com/direction/release/feature_flags/) within the GitLab product
    - [x] The [help page about the Feature Flags feature](https://gitlab.com/help/user/project/operations/feature_flags)
- [x] Read through the [Telemetry Guide](https://docs.gitlab.com/ee/development/telemetry/index.html)
- [x] Read through the [Usage Ping Guide](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html)
- [x] Read through the [Snowplow Guide](https://docs.gitlab.com/ee/development/telemetry/snowplow.html)
- [x] Read up on the conversation around [improving guidance for backstage & feature throughput types](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/488) ([reading journal notes](https://gitlab.com/dreedy/reading-journal/-/tree/master/gitlab-org/quality/team-tasks/488-Improve-guidance-for-backstage-and-feature-throughput-types))
- [x] Check out the [GitLab 101 Tool Certification](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/gitlab-101/)
- [x] Read the [Career Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/) page
- [x] Look over the [Sr. Engineer Coaching Template](https://docs.google.com/document/d/11xZpY2RuTldp1g6bHRFYKwwlQjNjYaPquLPo5uD6hrg/edit#)
- [x] Review the page on [Promotions & Transfers](https://about.gitlab.com/handbook/people-group/promotions-transfers/)
- [x] Review some of the [GitLab Competencies](https://about.gitlab.com/handbook/competencies/#list)
  - [x] Iteration (completed on 2020-07-01)
- [x] Check my Carta access (stock options)
  - I think I’ll receive an email, actually, when my stock options have been approved and my access to Carta is granted.
- [x] Read the [Halloway Guide to Equity Compensation](https://www.holloway.com/g/equity-compensation) (re: Stock Options)
- [x] Review [the Q/A doc](https://docs.google.com/document/d/1HWNifdWRG6Mm3lZjV_Tlqj_yLoWo2aamYkql_A2pPJY/edit#heading=h.mxhyv91rino6) from the Stock Options session
- [x] Read up on [GitLab’s stock option grants](https://about.gitlab.com/handbook/stock-options/)
- [x] Look into [creating a simple Slack app](https://api.slack.com/) that reports your own Slack statistics (e.g. how many messages sent, how many DMs sent, how many DMs read, etc.)

---

## 🙅 Tasks to let go of

Tasks that I may have been forwarding along or which I may have initially added to the “To do later” list which I no longer plan to do – if they happen, great, but I’m not actively planning to complete them.

- …

---

## See also

- [Growth:Conversion Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
- [Growth:Conversion Epics Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aconversion)

[%13.3]: https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion&milestone_title=13.3
[#219544]: https://gitlab.com/gitlab-org/gitlab/-/issues/219544
[#219944]: https://gitlab.com/gitlab-org/gitlab/-/issues/219944
[#8516]: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8516
[!36394]: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36394
[!723]: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/723
[my workflow]: https://docs.google.com/document/d/1sUcDknoHkVOIMhBZzpNPxx50h6YpWw-MMIfxdDirMfw/edit#
[status hero]: https://statushero.com/teams/gitlab/members/dallas-reedy
[Growth Team Day]: https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/175
