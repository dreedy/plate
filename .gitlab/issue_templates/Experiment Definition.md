<!--
Set the title of this issue to:
[EXP DEF] <Subject line for your experiment idea>
-->

# Experiment Definition

<!--
Don’t worry if you don’t have answers for all of the questions below. The primary objective in the early stages is to collect as much context & detail as we can so that we understand the proposed experiment.
-->

## High Level Overview

<!--
What is the experiment idea? Be sure to include the relevant context of the experiment, maybe a brief description of how the UX flows today vs. your proposed alterations, and describe your experiment idea in as much detail as you can.
-->

## Hypothesis

<!--
What is your hypothesis? What do you think that running the experiment you described will achieve? How will it improve the User Experience or the bottom line of the business?
-->

## Measurement

<!--
How will we measure the success of this experiment? What metrics will we need to collect? How will we determine success vs. failure (change in metrics over period of time).
-->

## To-Dos

- [ ] Filled in as much detail & context as possible
- [ ] Have a well-written, well-defined hypothesis
- [ ] Have specifics about what we will measure to determine success and what the timeframe is from launch to statistical significance
- [ ] Assigned this issue to the appropriate Project Manager (or your PM if you’re unsure of which one to assign it to)

/label ~"experiment" ~"experiment::idea" ~"growth experiment"
