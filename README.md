# :fork_knife_plate: My Plate

Where I track capacity, appetite, & commitments. A place where I can stay organized while also allowing transparency for my teams & anyone else who is interested in what I’m currently focused on.

- [Current Tasks →](Tasks/2020-week-34.md)
- [Current OKRs →](OKRs/fy21-q3.md)

## Guiding Principles

One of the main goals is to avoid some giant, never-ending task backlog, either within the current week’s “To do later” section, constantly being carried over week after week and growing in size, or in a separate `backlog.md`, or similar, file, also just growing in size and causing most tasks to be completely burried & lost. Either a task should be scheduled as “I plan to work on this” or it should be completely discarded (it can always come back later if it turns out to be important).

- If something is important enough to write down and keep track of, it’s important enough to schedule.
- If something would be scheduled out beyond the current week, just make the file for whichever week and add the task.
  - These future tasks can be added to the “To do later” list of the current week during the week and then moved to their appropriate place by the end of the week.
- This is an area that I am still actively learning & growing in. I will be patient with myself, allow myself grace, & give myself room to grow & experiment.

## :bulb: Inspiration

This project was heavily inspired by others. You should totally check them out for inspiration!

- [Tasks](https://gitlab.com/kcomoli/tasks) by [Kevin Comoli](https://gitlab.com/kcomoli)
- [Focus](https://gitlab.com/matejlatin/focus) by [Matej Latin](https://gitlab.com/matejlatin)

## :eyes: See Also

- [My Reading Journal](https://gitlab.com/dreedy/reading-journal)
- [My Personal README](https://gitlab.com/dreedy/readme)
